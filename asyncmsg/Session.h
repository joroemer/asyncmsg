#ifndef ASYNCMSG_SESSION_H
#define ASYNCMSG_SESSION_H

#include "asyncmsg/detail/Header.h"
#include "asyncmsg/Message.h"

#include <boost/asio/io_service.hpp>
#include <boost/asio/buffer.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/strand.hpp>
#include <cstdint>
#include <deque>
#include <memory>
#include <string>
#include <vector>
#include <iosfwd>                     // for size_t

namespace boost { namespace system { class error_code; } }

namespace daq
{
namespace asyncmsg
{

/*! \brief Represents a generic messaging session.
 *
 *  Applications are expected to inherit from this class, defining a specific communication
 *  protocol by implementing the pure virtual methods:
 *  \li onOpen() / onOpenError()
 *  \li createMessage() / onReceive() / onReceiveError()
 *  \li onSend() / onSendError()
 */
class Session:
    public std::enable_shared_from_this<Session>
{

public:

  /*! \brief Defines the possible Session states.
   */
  enum class State {
    CLOSED,
    OPEN_PENDING,
    OPEN,
    CLOSE_PENDING
  };

  /*! \brief Defines the upper limit for message type identifiers.
   *
   *  Message types in the range
   *  [ RESERVED_MESSAGE_TYPES_BASE : std::numeric_limits<std::uint32_t>::max() ]
   *  are reserved and must not be used.
   */
  static const std::uint32_t RESERVED_MESSAGE_TYPES_BASE = 0x80000000;

protected:

  /*! \brief Constructs a Session without opening it.
   *
   *  This constructor creates a Session without opening it. The session needs to be opened using
   *  asyncOpen() or accepted by a Server before messages can be sent or received.
   *
   *  \param[in] ioService The boost::asio::io_service that the session will use to perform I/O
   *      operations.
   *
   *  \remark Session objects must be managed by std::shared_ptr instances. Subclasses of Session
   *      should enforce this by making their constructor inaccessible and defining an appropriate
   *      factory method.
   */
  explicit Session(boost::asio::io_service& ioService);

public:

  virtual ~Session();

public:

  /*! \brief Gets the strand associated with the session.
   *
   *  This methods gives access to the boost::asio::io_service::strand object that the session uses
   *  to dispatch completion methods.
   */
  boost::asio::io_service::strand& getStrand();

public:

  /*! \brief Initiates an asynchronous open.
   *
   *  Asynchronously opens a client messaging session with a remote peer. When the session is open
   *  onOpen() will be invoked. If an error occurs, onOpenError() will be invoked instead \b and
   *  the sesssion will be closed immediately afterwards.
   *
   *  \param[in] localName Identifier of the local peer. It will be communicated to the remote
   *      peer.
   *  \param[in] remoteEndpoint IP address and TCP port of the remote peer.
   */
  void asyncOpen(const std::string& localName,
      const boost::asio::ip::tcp::endpoint& remoteEndpoint);

protected:

  /*! \brief Completion method for successful asyncOpen().
   *
   *  This method is called when an open operation completes successfully.
   */
  virtual void onOpen() = 0;

  /*! \brief Completion method for failed asyncOpen().
   *
   *  This method is called when an error occurs during an open operation.
   *
   *  \param[in] error Indicates what error occurred.
   */
  virtual void onOpenError(const boost::system::error_code& error) = 0;

public:

  /*! \brief Initiates an asynchronous receive.
   *
   *  Asynchronously receives a message from the remote peer. createMessage() will be invoked to
   *  allocate memory for the message being received. When the message is received successfully
   *  onReceive() will be invoked. If an error occurs, onReceiveError() will be invoked instead.
   */
  void asyncReceive();

protected:

  /*! \brief MutableMessage factory method.
   *
   *  This method is called to create a new "empty" MutableMessage instance in which message data
   *  will be stored.
   *
   *  \param[in] typeId The type ID of the message to be received.
   *  \param[in] transactionId The transaction ID of the message to be received.
   *  \param[in] size The size of the message to be received.
   *
   *  \returns A pointer to a MutableMessage instance suitable for storing \c size bytes. The
   *      Session takes ownership of the instance and returns it via onReceive() or
   *      onReceiveError(). Returning a null pointer instructs the session to discard the incoming
   *      message (note that in this case neither onReceive() nor onReceiveError() will be invoked)
   */
  virtual std::unique_ptr<InputMessage> createMessage(std::uint32_t typeId,
      std::uint32_t transactionId, std::uint32_t size) = 0;

  /*! \brief Completion method for successful asyncReceive().
   *
   *  This method is called when a receive operation completes successfully.
   *
   *  \param[in] message Pointer to the received message.
   */
  virtual void onReceive(std::unique_ptr<InputMessage> message) = 0;

  /*! \brief Completion method for failed asyncReceive().
   *
   *  This method is called when an error occurs during a receive operation.
   *
   *  \param[in] error Indicates what error occurred.
   *  \param[in] message Pointer to the message into which data was being received. This pointer is
   *      null if the failure occurred before a message was created by createMessage().
   */
  virtual void onReceiveError(const boost::system::error_code& error,
      std::unique_ptr<InputMessage> message) = 0;

public:

  /*! \brief Initiates an asynchronous send.
   *
   *  Asynchronously sends a message to the remote peer. When the message is received successfully
   *  onSend() will be invoked. If an error occurs, onSendError() will be invoked instead.
   *
   *  \param[in] message Pointer to the message to send. The Session takes ownership of message and
   *      returns it via onSend() or onSendError().
   */
  void asyncSend(std::unique_ptr<const OutputMessage> message);

protected:

  /*! \brief Completion method for successful asyncSend().
   *
   *  This method is called when a send operation completes successfully.
   *
   *  \param[in] message Pointer to the sent message.
   */
  virtual void onSend(std::unique_ptr<const OutputMessage> message) = 0;

  /*! \brief Completion method for failed asyncSend().
   *
   *  This method is called when an error occurs during a send operation.
   *
   *  \param[in] error Indicates what error occurred.
   *  \param[in] message Pointer to the message being sent.
   */
  virtual void onSendError(const boost::system::error_code& error,
      std::unique_ptr<const OutputMessage> message) = 0;

public:

  /*! \brief Initiates an asynchronous close.
   *
   *  Asynchronously closes the session. Pending asynchronous operations will be canceled and the
   *  corresponding "failed" completion methods will be invoked with the
   *  boost::asio::error::operation_aborted error. When all pending completion methods have been
   *  executed, onClose() will be invoked. If an error occurs, onCloseError() will be invoked
   *  instead.
   */
  void asyncClose();

protected:

  /*! \brief Completion method for successful asyncClose().
   *
   *  This method is called when a close operation completes successfully.
   */
  virtual void onClose() = 0;

  /*! \brief Completion method for failed asyncClose().
   *
   *  This method is called when an error occurs during a close operation.
   *
   *  \param[in] error Indicates what error occurred.
   */
  virtual void onCloseError(const boost::system::error_code& error) = 0;

public:

  /*! \brief Returns the IP address and TCP port of the local peer, if any.
   */
  boost::asio::ip::tcp::endpoint localEndpoint() const;

  /*! \brief Returns the identifier of the local peer, if any.
   */
  std::string localName() const;

  /*! \brief Returns the IP address and TCP port of the remote peer, if any.
   */
  boost::asio::ip::tcp::endpoint remoteEndpoint() const;

  /*! \brief Returns the identifier of the remote peer, if any.
   */
  std::string remoteName() const;

  /*! \brief Returns the state of the session.
   */
  State state() const;

private:

  friend class Server;

  void startOpen(const std::shared_ptr<Session>& self);
  void abortOpen(const boost::system::error_code& error);
  void checkClose();
  void startReceive(const std::shared_ptr<Session>& self);
  void startSend(const std::shared_ptr<Session>& self);

  boost::asio::io_service::strand m_strand;

  std::string m_localName;
  std::string m_remoteName;
  boost::asio::ip::tcp::socket m_socket;
  State m_state;

  std::vector<boost::asio::mutable_buffer> m_recvBuffers;
  detail::Header m_recvHeader;
  std::unique_ptr<InputMessage> m_recvMessage;
  std::size_t m_recvNPending;

  std::vector<boost::asio::const_buffer> m_sendBuffers;
  detail::Header m_sendHeader;
  std::deque<std::unique_ptr<const OutputMessage>> m_sendQueue;

};

} // namespace asyncmsg
} // namespace daq

#endif // !defined(ASYNCMSG_SESSION_H)
