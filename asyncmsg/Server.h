#ifndef ASYNCMSG_SERVER_H
#define ASYNCMSG_SERVER_H

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/strand.hpp>
#include <atomic>
#include <cstdint>
#include <memory>
#include <string>

namespace boost { namespace system { class error_code; } }

namespace daq
{
namespace asyncmsg
{

class Session;

/*! \brief Represents a generic messaging server.
 *
 *  Applications are expected to inherit from this class, implementing the pure virtual methods:
 *  \li onAccept() / onAcceptError()
 */
class Server :
    public std::enable_shared_from_this<Server>
{

protected:

  /*! \brief Constructs a Server without opening it.
   *
   *  This constructor creates a server without listening for incoming sessions. The server needs
   *  to be opened using listen().
   *
   *  \param[in] ioService The boost::asio::io_service that the server will use to perform I/O
   *      operations.
   *
   *  \remark Server objects must be managed by std::shared_ptr instances. Subclasses of Server
   *      should enforce this by making their constructor inaccessible and defining an appropriate
   *      factory method.
   */
  Server(boost::asio::io_service& ioService);

public:

  virtual ~Server();

public:

  /*! \brief Gets the strand associated with the server.
   *
   *  This methods gives access to the boost::asio::io_service::strand object that the server uses
   *  to dispatch completion methods.
   */
  boost::asio::io_service::strand& getStrand();

public:

  /*! \brief Starts listening for incoming sessions.
   *
   *  \param[in] localName Identifier of the local peer. It will be communicated to the connecting
   *      remote peers.
   *  \param[in] localEndpoint IP address and TCP port on which the server will listen.
   */
  void listen(const std::string& localName, const boost::asio::ip::tcp::endpoint& localEndpoint =
      boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v6(), 0));

public:

  /*! \brief Initiates an asynchronous accept.
   *
   *  Asynchronously accepts an incoming session with a remote peer. When the session is accepted,
   *  onAccept() will be invoked. If an error occurs, onAcceptError() will be invoked instead.
   *
   *  \param[in] session Pointer to a Session instance suitable for handling the incoming session
   *      (i.e. with session->state() == Session::State::CLOSED). On successful accept, the server
   *      opens the session as if Session::asyncOpen() was called.
   */
  void asyncAccept(std::shared_ptr<Session> session);

protected:

  /*! \brief Completion method for successful asyncAccept().
   *
   *  This method is called when an accept operation completes successfully.
   *
   *  \param[in] session Pointer to the accepted session.
   */
  virtual void onAccept(std::shared_ptr<Session> session) = 0;

  /*! \brief Completion method for failed asyncAccept().
   *
   *  This method is called when an error occurs during an accept operation.
   *
   *  \param[in] error Indicates what error occurred.
   *  \param[in] session Pointer to the session being accepted.
   */
  virtual void onAcceptError(const boost::system::error_code& error,
      std::shared_ptr<Session> session) = 0;

public:

  /*! \brief Stops listening for incoming connections.
   *
   *  This method immediately shuts down the server. Pending asynchronous operations will be
   *  canceled and the corresponding "failed" completion methods will be invoked with the
   *  boost::asio::error::operation_aborted error.
   */
  void close();

public:

  /*! \brief Returns the IP address and TCP port on which the server listens.
   */
  boost::asio::ip::tcp::endpoint localEndpoint() const;

  /*! \brief Returns the identifier of the local peer, if any.
   */
  std::string localName() const;

private:

  void startAccept(const std::shared_ptr<Server>& self, const std::shared_ptr<Session>& session);

  boost::asio::ip::tcp::acceptor m_acceptor;
  std::string m_localName;
  std::atomic<bool> m_isListening;
  boost::asio::io_service::strand m_strand;

};

} // namespace asyncmsg
} // namespace daq

#endif // !defined(ASYNCMSG_SERVER_H)
