#ifndef ASYNCMSG_ERROR_H
#define ASYNCMSG_ERROR_H

#include <string>
#include <boost/system/error_code.hpp>

namespace daq
{
namespace asyncmsg
{

enum class Error:
    int
{
  SESSION_NOT_OPEN,
  SESSION_NOT_CLOSED,
  RESERVED_MESSAGE_TYPE,
  UNEXPECTED_MESSAGE_TYPE,
  MESSAGE_DISCARDED
};

class ErrorCategory:
    public boost::system::error_category
{

public:

  static ErrorCategory& instance();

  const char* name() const noexcept;
  std::string message(int value) const;

private:

  static ErrorCategory s_instance;

};

// Helper function used by the constructor of boost::system::error_code via ADL
inline boost::system::error_code make_error_code(Error e)
{
  return boost::system::error_code(static_cast<int>(e), ErrorCategory::instance());
}

} // namespace asyncmsg
} // namespace daq

namespace boost
{
namespace system
{

// Register our Error enum with Boost.System
template<> struct is_error_code_enum<daq::asyncmsg::Error>
{
  static const bool value = true;
};

} // namespace system
} // namespace boost

#endif // !defined(ASYNCMSG_ERROR_H)
