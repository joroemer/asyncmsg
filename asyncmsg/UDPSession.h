// this is -*- c++ -*-
#ifndef ASYNCMSG_UDPSESSION_H
#define ASYNCMSG_UDPSESSION_H

#include "asyncmsg/detail/Header.h"
#include "asyncmsg/Message.h"

#include <boost/asio/io_service.hpp>
#include <boost/asio/buffer.hpp>
#include <boost/asio/ip/address.hpp>  // for address
#include <boost/asio/ip/udp.hpp>
#include <boost/asio/strand.hpp>

#include <cstdint>
#include <deque>
#include <memory>
#include <string>
#include <vector>
#include <iosfwd>                     // for size_t

namespace boost { namespace system { class error_code; } }

namespace daq
{
namespace asyncmsg
{

/*! \brief Represents a generic messaging session using UDP.
 *
 *  Applications are expected to inherit from this class, defining a specific communication
 *  protocol by implementing the pure virtual methods:
 *
 *  \li createMessage() / onReceive() / onReceiveError()
 *  \li onSend() / onSendError()
 */
class UDPSession:
    public std::enable_shared_from_this<UDPSession>
{

public:

  /*! \brief Defines the possible Session states.
   */
  enum class State {
    CLOSED,
    OPEN
  };

  /*! \brief Defines the upper limit for message type identifiers.
   *
   *  Message types in the range
   *  [ RESERVED_MESSAGE_TYPES_BASE : std::numeric_limits<std::uint32_t>::max() ]
   *  are reserved and must not be used.
   */
  static const std::uint32_t RESERVED_MESSAGE_TYPES_BASE = 0x80000000;

protected:

  /*! \brief Constructs a UDPSession and opens it.
   *
   *  This constructor creates a UDPSession. The session needs to be connected using
   *  connect() before sending but is ready for receiving.
   *
   *  \param[in] ioService The boost::asio::io_service that the session will use to perform I/O
   *      operations.
   *  \param[in] port The port number to bind the socket to. A port number of zero means use
   *      a system assigned port.
   *
   *  \remark Session objects must be managed by std::shared_ptr instances. Subclasses of Session
   *      should enforce this by making their constructor inaccessible and defining an appropriate
   *      factory method.
   */
  UDPSession(boost::asio::io_service& ioService, uint16_t port = 0);

public:

  virtual ~UDPSession();

public:

  /*! \brief Gets the strand associated with the session.
   *
   *  This methods gives access to the boost::asio::io_service::strand object that the session uses
   *  to dispatch completion methods.
   */
  boost::asio::io_service::strand& getStrand();

public:

  /*! \brief Synchronously connects a client messaging session with a remote peer. 
   *
   *  Note that a UDPSession can be re-connected multiple times with different endpoints.
   *
   *  \param[in] remoteEndpoint IP address and UDP port of the remote peer.
   *
   *  \remark Throws on error.
   */
  void connect(const boost::asio::ip::udp::endpoint& remoteEndpoint);

public:

  /*! \brief Initiates an asynchronous receive.
   *
   *  Asynchronously receives a message from the remote peer. createMessage() will be invoked to
   *  allocate memory for the message being received. When the message is received successfully
   *  onReceive() will be invoked. If an error occurs, onReceiveError() will be invoked instead.
   */
  void asyncReceive();

protected:

  /*! \brief MutableMessage factory method.
   *
   *  This method is called to create a new "empty" MutableMessage instance in which message data
   *  will be stored.
   *
   *  \param[in] typeId The type ID of the message to be received.
   *  \param[in] transactionId The transaction ID of the message to be received.
   *  \param[in] size The size of the message to be received.
   *
   *  \returns A pointer to a MutableMessage instance suitable for storing \c size bytes. The
   *      Session takes ownership of the instance and returns it via onReceive() or
   *      onReceiveError(). Returning a null pointer instructs the session to discard the incoming
   *      message (note that in this case neither onReceive() nor onReceiveError() will be invoked)
   */
  virtual std::unique_ptr<InputMessage> createMessage(std::uint32_t typeId,
      std::uint32_t transactionId, std::uint32_t size) = 0;

  /*! \brief Completion method for successful asyncReceive().
   *
   *  This method is called when a receive operation completes successfully.
   *
   *  \param[in] message Pointer to the received message.
   */
  virtual void onReceive(std::unique_ptr<InputMessage> message) = 0;

  /*! \brief Completion method for failed asyncReceive().
   *
   *  This method is called when an error occurs during a receive operation.
   *
   *  \param[in] error Indicates what error occurred.
   *  \param[in] message Pointer to the message into which data was being received. This pointer is
   *      null if the failure occurred before a message was created by createMessage().
   */
  virtual void onReceiveError(const boost::system::error_code& error,
      std::unique_ptr<InputMessage> message) = 0;

public:

  /*! \brief Initiates an asynchronous send.
   *
   *  Asynchronously sends a message to the remote peer. When the message is received successfully
   *  onSend() will be invoked. If an error occurs, onSendError() will be invoked instead.
   *
   *  \param[in] message Pointer to the message to send. The Session takes ownership of message and
   *      returns it via onSend() or onSendError().
   */
  void asyncSend(std::unique_ptr<const OutputMessage> message);

protected:

  /*! \brief Completion method for successful asyncSend().
   *
   *  This method is called when a send operation completes successfully.
   *
   *  \param[in] message Pointer to the sent message.
   */
  virtual void onSend(std::unique_ptr<const OutputMessage> message) = 0;

  /*! \brief Completion method for failed asyncSend().
   *
   *  This method is called when an error occurs during a send operation.
   *
   *  \param[in] error Indicates what error occurred.
   *  \param[in] message Pointer to the message being sent.
   */
  virtual void onSendError(const boost::system::error_code& error,
      std::unique_ptr<const OutputMessage> message) = 0;

public:

  /*! \brief Closes the session.
   *
   *  This method immediately closes the session. Pending asynchronous operations will be canceled
   *  and the corresponding "failed" completion methods will be invoked with the
   *  boost::asio::error::operation_aborted error.
   */
  void close();

public:

  /*! \brief Returns the IP address and UDP port of the local peer, if any.
   */
  boost::asio::ip::udp::endpoint localEndpoint() const;

  /*! \brief Returns the IP address and UDP port of the remote peer, if any.
   */
  boost::asio::ip::udp::endpoint remoteEndpoint() const;

  /*! \brief Returns the state of the session.
   */
  State state() const;

public:

    /*! \brief Set the outgoing interface if this session is used with an UDP multicast address.
     */
    void setOutgoingInterface(const boost::asio::ip::address& ip_address);

    /*! \brief Join a multicast group.
     */
    void joinMulticastGroup(const boost::asio::ip::address& mc_address, const boost::asio::ip::address& network = boost::asio::ip::address());

    /*! \brief Leave a multicast group.
     */
    void leaveMulticastGroup(const boost::asio::ip::address& mc_address);

private:

  void startReceive(const std::shared_ptr<UDPSession>& self);
  void startSend(const std::shared_ptr<UDPSession>& self);

  boost::asio::io_service::strand m_strand;

  boost::asio::ip::udp::socket m_socket;
  State m_state;

  std::vector<boost::asio::mutable_buffer> m_recvBuffers;
  detail::Header m_recvHeader;
  std::unique_ptr<InputMessage> m_recvMessage;
  std::size_t m_recvNPending;

  std::vector<boost::asio::const_buffer> m_sendBuffers;
  detail::Header m_sendHeader;
  std::deque<std::unique_ptr<const OutputMessage>> m_sendQueue;

};

} // namespace asyncmsg
} // namespace daq

#endif // !defined(ASYNCMSG_SESSION_H)
