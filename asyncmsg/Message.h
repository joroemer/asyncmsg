#ifndef ASYNCMSG_MESSAGE_H
#define ASYNCMSG_MESSAGE_H

#include <cstdint>
#include <vector>
#include <boost/asio/buffer.hpp>

namespace daq
{
namespace asyncmsg
{

//! Interface representing a generic message.
class Message
{
public:

  //! Returns the message's type ID.
  virtual std::uint32_t typeId() const = 0;

  //! Returns the message's transaction ID, if any.
  virtual std::uint32_t transactionId() const = 0;

  virtual ~Message()
  {
  }

};

//! Interface representing a generic message providing write access to its data.
class InputMessage :
    public virtual Message
{
public:

  /*! \brief Provides write access to the message via a sequence of boost::asio::mutable_buffer
   *  objects.
   *
   *  \param[out] buffers A sequence of boost::asio::mutable_buffer objects pointing to the message
   *      data are pushed back into this vector.
   */
  virtual void toBuffers(std::vector<boost::asio::mutable_buffer>& buffers) = 0;

  virtual ~InputMessage()
  {
  }

};

//! Interface representing a generic message providing read access to its data.
class OutputMessage :
    public virtual Message
{
public:

  /*! \brief Provides read access to the message via a sequence of boost::asio::const_buffer
   * objects.
   *
   *  \param[out] buffers A sequence of boost::asio::const_buffer objects pointing to the message
   *      data are pushed back into this vector.
   */
  virtual void toBuffers(std::vector<boost::asio::const_buffer>& buffers) const = 0;

  virtual ~OutputMessage()
  {
  }

};

} // namespace asyncmsg
} // namespace daq

#endif // !defined(ASYNCMSG_MESSAGE_H)
