
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netinet/ether.h>
#include <ifaddrs.h>
#include <netdb.h>
#include "Interface.h"

#include <iostream>

namespace daq {

    namespace asyncmsg {

        class Cleanup {
        public:
            ~Cleanup();
        };

        // static
        std::vector<const Interface *> Interface::s_interfaces;
        struct ifaddrs *Interface::s_addresses = 0;

        Cleanup s_cleanup;

        // Interface::Cleanup s_cleanup;

        /**
         * Information about a network interface.
         *
         * This basically encapsulates the information returned
         * by various ioctls() in a 'struct ifreq'.
         *
         * Only the IPv4 address family is supported at
         * the moment and only Ethernet HW addresses are known.
         */

        /** The interface name, e.g. eth0 */
        const std::string Interface::name() const
        {
            return m_addr->ifa_name;
        }

        // Return the address family.
        int Interface::family() const
        {
            return (m_addr->ifa_addr ? m_addr->ifa_addr->sa_family : AF_UNSPEC);
        }

        /** The internal interface index (usually starting at 1) */
        int Interface::index() const
        {
            return if_nametoindex(m_addr->ifa_name);
        }

        /** The various flags in net/if.h for interfaces */
        int Interface::flags() const
        {
            return m_addr->ifa_flags;
        }

        /** Return true if the corresponding flag from net/if.h is set */
        bool Interface::has_flag(int flag) const
        {
            return m_addr->ifa_flags & flag;
        }

        /** The IP address of this interface or 0 */
        in_addr_t Interface::address() const
        {
            if(m_addr->ifa_addr && (m_addr->ifa_addr->sa_family == AF_INET)) {
                return ((sockaddr_in *)m_addr->ifa_addr)->sin_addr.s_addr;
            } else {
                return 0;
            }
        }

        /** The IP address of this interface or 0 */
        in6_addr Interface::address_v6() const
        {
            if(m_addr->ifa_addr && (m_addr->ifa_addr->sa_family == AF_INET6)) {
                return ((sockaddr_in6 *)m_addr->ifa_addr)->sin6_addr;
            } else {
                return in6addr_any;
            }
        }

        /** The IP address of this interface as a string or "" */
        std::string Interface::address_string() const
        {
            char buf[INET6_ADDRSTRLEN+1]= { 0 };
            if(m_addr->ifa_addr) {
                getnameinfo(m_addr->ifa_addr, sizeof(sockaddr_storage), buf, sizeof(buf), 0, 0, NI_NUMERICHOST);
            }
            return buf;
        }

        /** The broadcast IP address of this interface or 0 */
        in_addr_t Interface::broadcast() const
        {
            if(m_addr->ifa_addr && (m_addr->ifa_addr->sa_family == AF_INET)) {
                return ((sockaddr_in *)m_addr->ifa_broadaddr)->sin_addr.s_addr;
            } else {
                return 0;
            }
        }

        /** The broadcast IP address as a string or "" */
        std::string Interface::broadcast_string() const
        {
            char buf[INET_ADDRSTRLEN] = { 0 };
            if(m_addr->ifa_addr && (m_addr->ifa_addr->sa_family == AF_INET && m_addr->ifa_broadaddr)) {
                getnameinfo(m_addr->ifa_broadaddr, sizeof(sockaddr_in), buf, sizeof(buf), 0, 0, NI_NUMERICHOST);
            }
            return buf;
        }


        /** The netmask for this interface or 0 */
        in_addr_t Interface::netmask() const
        {
            return ((sockaddr_in *)m_addr->ifa_netmask)->sin_addr.s_addr;
        }

        /** The IP v6 netmask for this interface  */
        in6_addr Interface::netmask_v6() const
        {
            return ((sockaddr_in6 *)m_addr->ifa_netmask)->sin6_addr;
        }

        /** The netmask for this interface or 0 */
        std::string Interface::netmask_string() const
        {
            char buf[INET6_ADDRSTRLEN+1]= { 0 };
            getnameinfo(m_addr->ifa_netmask, sizeof(sockaddr_storage), buf, sizeof(buf), 0, 0, NI_NUMERICHOST);
            return buf;
        }

        /** Network: address masked with netmask */
        in_addr_t Interface::network() const
        {
            return  ((sockaddr_in *)m_addr->ifa_addr)->sin_addr.s_addr & ((sockaddr_in *)m_addr->ifa_netmask)->sin_addr.s_addr;
        }

        /** Network: address masked with netmask, IPV6*/
        in6_addr Interface::network_v6() const
        {
            in6_addr result = ((sockaddr_in6 *)m_addr->ifa_addr)->sin6_addr;
            for(int i = 0; i < 4; i++) {
                result.s6_addr32[i] &= ((sockaddr_in6 *)m_addr->ifa_netmask)->sin6_addr.s6_addr32[i];
            }
            return result;
        }


#if 0
        /** The Ethernet hw address of the interface */
        ether_addr Interface::hw_address() const
        {
            ether_addr addr;
            memcpy(&addr, m_addr->ifa_addr->sa_data, sizeof(ether_addr));
            return addr;
        }

        std::string Interface::hw_address_string() const
        {
            char buffer[20];
            return ether_ntoa_r((const ether_addr *)m_addr->ifa_addr->sa_data, buffer);
        }
#endif 

        /** Find interface by index.*/ 
        const Interface *Interface::find(int index, int af)
        {
            const InterfaceList& if_list = interfaces();
            for(InterfaceList::const_iterator it = if_list.begin();
                it != if_list.end();
                ++it) {
                if((*it)->index() == index && (*it)->family() == af) return *it;
            }
            return 0;
        }

        /// Find interface by name.
        const Interface *Interface::find(const std::string& name, int af)
        {
            const InterfaceList& if_list = interfaces();
            for(InterfaceList::const_iterator it = if_list.begin();
                it != if_list.end();
                ++it) {
                if((*it)->name() == name && (*it)->family() == af) return *it;
            }
            return 0;
        }

        /// Return list of all interfaces.
        const Interface::InterfaceList& Interface::interfaces()
        {
            if(s_interfaces.size() == 0) {

                getifaddrs(&s_addresses);
                struct ifaddrs *ptr = s_addresses;

                while(ptr) {
                    if(ptr->ifa_addr && (ptr->ifa_addr->sa_family == AF_INET || ptr->ifa_addr->sa_family == AF_INET6)) {
                        s_interfaces.push_back(new Interface(ptr));
                    }
                    ptr = ptr->ifa_next;
                }
            }

            return s_interfaces;
        }

        Interface::Interface(ifaddrs *addr)
            : m_addr(addr)
        {
        }
    
        Interface::~Interface()
        {
        }

        Cleanup::~Cleanup()
        {
            for(Interface::InterfaceList::const_iterator it = Interface::s_interfaces.begin();
                it != Interface::s_interfaces.end();
                ++it) {
                delete *it;
            }
            freeifaddrs(Interface::s_addresses);
        }

        std::ostream& operator<<(std::ostream& os, const Interface& intf)
        {
            switch (intf.family()) {
            case AF_INET:
            case AF_INET6:
                os << intf.address_string() << '/' << intf.netmask_string();
                break;
            default:
                os << intf.name() << ": " << "Unkwown interface type: " << intf.family() ;
                break;
            }

            return os;
        }

        bool operator==(const in6_addr& a, const in6_addr& b)
        {
            return memcmp(&a,&b, sizeof(in6_addr)) == 0;
        }
        
    }
}

