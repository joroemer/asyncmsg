// this is -*- c++ -*-
#ifndef TRANSPORT_INTERFACE_H_
#define TRANSPORT_INTERFACE_H_

#include <string>
#include <vector>
#include <iosfwd>

#include <net/if.h>
#include <netinet/in.h>
#include <net/ethernet.h>

// fwd declaration
struct ifaddrs;

namespace daq {
    namespace asyncmsg {

        // Internal helper class
        class Cleanup;

        /**
         * Information about a network interface.
         *
         * This basically encapsulates the information returned
         * by getifaddrs().
         *
         * Both IP V4 and V6 are supported. 
         */
        class Interface {
        public:

            /** The interface name, e.g. eth0 */
            const std::string name() const;

            /** The address family for this Interface 
             *  One of AF_INET, AF_INET6, AF_PACKET
             */
            int family() const;

            /** The internal interface index (usually starting at 1) */
            int index() const;

            /** The various flags in net/if.h for interfaces */
            int flags() const;

            /** Return true if the corresponding flag from net/if.h is set */
            bool has_flag(int flag) const;

            /** The IP address of this interface or 0 */
            in_addr_t address() const;

            /** The IP V6 address of this interface */
            in6_addr  address_v6() const;

            /** The IP address of this interface as a string or "" */
            std::string address_string() const;

            /** The broadcast IP address of this interface or 0 */
            in_addr_t broadcast() const;

            /** The broadcast IP address as a string or "" */
            std::string broadcast_string() const;

            /** The netmask for this interface or 0 */
            in_addr_t netmask() const;

            /** The IP V6 netmask of this interface */
            in6_addr  netmask_v6() const;

            /** Network: address masked with netmask */
            in_addr_t network() const;

            /** Network: address masked with netmask, IPV6*/
            in6_addr network_v6() const;

            /** The netmask for this interface or 0 */
            std::string netmask_string() const;

#if 0
            /** The Ethernet hw address of the interface */
            ether_addr hw_address() const;

            std::string hw_address_string() const;
#endif
        public:
            /** Find interface by index.*/ 
            static const Interface *find(int index, int af = AF_INET); 

            /// Find interface by name.
            static const Interface *find(const std::string& name, int af = AF_INET);

            typedef std::vector<const Interface *> InterfaceList;

            /// Return list of all interfaces.
            static const InterfaceList& interfaces();

        private:
            explicit Interface(struct ifaddrs *addr);
            ~Interface();

            // Representation
            struct ifaddrs *m_addr;

            friend class Cleanup;
        
            /// Global list of all interfaces
            static InterfaceList  s_interfaces;
            static struct ifaddrs *s_addresses;
        };

        std::ostream& operator<<(std::ostream& os, const Interface& );

        bool operator==(const in6_addr& , const in6_addr& );
    }
}

#endif // TRANSPORT_INTERFACE_H_
