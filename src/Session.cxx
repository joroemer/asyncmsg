#include "asyncmsg/Session.h"

#include "asyncmsg/Error.h"

#include <boost/asio/io_context_strand.hpp>     // for io_context::strand
#include <boost/asio/socket_base.hpp>           // for socket_base::keep_alive
#include <boost/cstdint.hpp>                    // for uint8_t
#include <boost/system/error_code.hpp>          // for error_code

#include <boost/asio/error.hpp>
#include <boost/asio/read.hpp>
#include <boost/asio/write.hpp>
#include <boost/range/iterator_range.hpp>
#include <cstdlib>
#include <limits>
#include <sstream>
#include <tuple>
#include <utility>
#include <algorithm>                            // for copy, copy_backward

#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/socket.h>

namespace {

/* Parse the TDAQ_ASYNCMSG_KEEPALIVE environment variable.
 *
 * TDAQ_ASYNCMSG_KEEPALIVE has the following format:
 * <enable>:<timeout_s>:<interval_s>:<max_failures>
 * where:
 * - <enable> (boolean, default: false)
 *   enables or disables TCP keep-alive probes
 * - <idle_s> (integer, default: /proc/sys/net/ipv4/tcp_keepalive_time)
 *   is the number of seconds a connection needs to be idle before TCP begins
 *   sending out keep-alive probes
 * - <interval_s> (integer, default: /proc/sys/net/ipv4/tcp_keepalive_intvl)
 *   is the number of seconds between TCP keep-alive probes
 * - <max_failures> (integer, default: /proc/sys/net/ipv4/tcp_keepalive_probes)
 *   is the maximum number of TCP keep-alive probes to send before giving up
 *   and killing the connection if no response is obtained
 * All fields are optional.
 */
std::tuple<bool, int, int, int> getKeepAliveOptions()
{
  bool enabled = false;
  int idle_s = 0, interval_s = 0, maxFailures = 0;
  char* env = std::getenv("TDAQ_ASYNCMSG_KEEPALIVE");
  if (env != nullptr) {
    std::istringstream ss{std::string(env)};
    ss >> enabled;
    ss.clear();
    ss.ignore(std::numeric_limits<std::streamsize>::max(), ':');
    ss >> idle_s;
    ss.clear();
    ss.ignore(std::numeric_limits<std::streamsize>::max(), ':');
    ss >> interval_s;
    ss.clear();
    ss.ignore(std::numeric_limits<std::streamsize>::max(), ':');
    ss >> maxFailures;
  }
  return std::make_tuple(enabled, idle_s, interval_s, maxFailures);
}

} // anonymous namespace


namespace daq
{
namespace asyncmsg
{

static const std::uint32_t HELLO_MESSAGE_ID = Session::RESERVED_MESSAGE_TYPES_BASE + 1;

Session::Session(boost::asio::io_service& ioService) :
    m_strand(ioService),
    m_socket(ioService),
    m_state(State::CLOSED),
    m_recvNPending(0),
    m_sendBuffers{ boost::asio::buffer(m_sendHeader.data()) }
{
}

Session::~Session()
{
}

void Session::asyncOpen(const std::string& localName,
    const boost::asio::ip::tcp::endpoint& remoteEndpoint)
{
  auto self = shared_from_this();
  m_strand.dispatch(
      [this, self, localName, remoteEndpoint] () {
        if (m_state != State::CLOSED) {
          // completion method must be posted
          m_strand.post(
              [this, self] () {
                onOpenError(Error::SESSION_NOT_CLOSED);
              });
        }
        else {
          m_state = State::OPEN_PENDING;
          m_localName = localName;
          m_socket.async_connect(remoteEndpoint, m_strand.wrap(
              [this, self] (const boost::system::error_code& error) {
                if (error) {
                  abortOpen(error);
                }
                else {
                  startOpen(self);
                }
              }));
        }
      });
}

void Session::startOpen(const std::shared_ptr<Session>& self)
{

  // 1. Set TCP options
  boost::system::error_code ignoredError;
  // Disable Nagle algorithm
  m_socket.set_option(boost::asio::ip::tcp::no_delay(true), ignoredError);
  // Enable keep-alive probes (if requested)
  {
    bool enabled;
    int idle_s, interval_s, maxFailures;
    std::tie(enabled, idle_s, interval_s, maxFailures) = getKeepAliveOptions();
    if (enabled) {
      m_socket.set_option(boost::asio::socket_base::keep_alive(true), ignoredError);
      if (idle_s > 0)
        ::setsockopt(m_socket.native_handle(), IPPROTO_TCP, TCP_KEEPIDLE,
                     &idle_s, sizeof(idle_s));
      if (interval_s > 0)
        ::setsockopt(m_socket.native_handle(), IPPROTO_TCP, TCP_KEEPINTVL,
                     &interval_s, sizeof(interval_s));
      if (maxFailures > 0)
        ::setsockopt(m_socket.native_handle(), IPPROTO_TCP, TCP_KEEPCNT,
                     &maxFailures, sizeof(maxFailures));
    }
  }


  // 2. Send the HELLO message
  // m_sendBuffers[0] points to m_sendHeader.data()
  m_sendBuffers.resize(1);
  m_sendHeader.fill(HELLO_MESSAGE_ID, 0, m_localName.size());
  m_sendBuffers.emplace_back(boost::asio::buffer(m_localName));
  boost::asio::async_write(m_socket, boost::make_iterator_range(m_sendBuffers), m_strand.wrap(
      [this, self] (const boost::system::error_code& error, std::size_t) {
        if (error) {
          abortOpen(error);
          return;
        }
        // 3. Receive the HELLO message header
        boost::asio::async_read(m_socket, boost::asio::buffer(m_recvHeader.data()), m_strand.wrap(
            [this, self] (const boost::system::error_code& error, std::size_t) {
              if (error) {
                abortOpen(error);
                return;
              }
              if (m_recvHeader.typeId() != HELLO_MESSAGE_ID) {
                abortOpen(Error::UNEXPECTED_MESSAGE_TYPE);
                return;
              }
              // 4. Receive the HELLO message body
              auto storage = std::make_shared<std::vector<std::uint8_t>>(m_recvHeader.size());
              boost::asio::async_read(m_socket, boost::asio::buffer(*storage), m_strand.wrap(
                  [this, self, storage] (const boost::system::error_code& error, std::size_t) {
                    if (error) {
                      abortOpen(error);
                      return;
                    }
                    m_remoteName.insert(m_remoteName.begin(), storage->begin(), storage->end());
                    m_state = State::OPEN;
                    onOpen();
                  }));
            }));
      }));
}

void Session::abortOpen(const boost::system::error_code& error)
{
  m_localName.clear();
  m_remoteName.clear();

  // abortOpen() is called if there was an error in one of the I/O operations in asyncOpen() or
  // startOpen(). We have to distinguish two cases:
  //  1) The error occurred because asyncClose() closed m_socket
  //  2) There was a "real" I/O error
  if (m_state == State::CLOSE_PENDING) {
    // case 1)
    onOpenError(error);
    m_state = State::CLOSED;
    onClose();
  }
  else {
    // case 2)
    boost::system::error_code ignoredError;
    m_socket.close(ignoredError);
    m_state = State::CLOSED;
    onOpenError(error);
  }
}


void Session::asyncClose()
{
  auto self = shared_from_this();
  m_strand.dispatch(
      [this, self] () {
        if (m_state != State::OPEN && m_state != State::OPEN_PENDING) {
          // completion method must be posted
          m_strand.post(
              [this, self] () {
                onCloseError(Error::SESSION_NOT_OPEN);
              });
        }
        else {
          m_state = State::CLOSE_PENDING;
          boost::system::error_code ignoredError;
          m_socket.close(ignoredError);
          // checkClose() calls a completion method, which must be posted
          m_strand.post(
              [this, self] () {
                checkClose();
              });
        }
      });
}

void Session::checkClose()
{
  if (m_state == State::CLOSE_PENDING && m_recvNPending == 0 && m_sendQueue.empty()) {
    m_state = State::CLOSED;
    onClose();
    m_localName.clear();
    m_remoteName.clear();
  }
}

void Session::asyncReceive()
{
  auto self = shared_from_this();
  m_strand.dispatch(
      [this, self] () {
        if (m_state != State::OPEN) {
          // completion method must be posted
          m_strand.post(
              [this, self] () {
                onReceiveError(Error::SESSION_NOT_OPEN, std::unique_ptr<InputMessage>());
              });
        }
        else {
          if (++m_recvNPending == 1) {
            // no previous receive operations were pending: start a new one
            startReceive(self);
          }
        }
      });
}

void Session::startReceive(const std::shared_ptr<Session>& self)
{
  boost::asio::async_read(m_socket, boost::asio::buffer(m_recvHeader.data()), m_strand.wrap(
      [this, self] (const boost::system::error_code& error, std::size_t) {
        if (error) {
          onReceiveError(error, std::unique_ptr<InputMessage>());
        }
        else {
          m_recvMessage = createMessage(
              m_recvHeader.typeId(), m_recvHeader.transactionId(), m_recvHeader.size());
          if (m_recvMessage) {
            m_recvBuffers.clear();
            m_recvMessage->toBuffers(m_recvBuffers);
            boost::asio::async_read(m_socket, boost::make_iterator_range(m_recvBuffers), m_strand.wrap(
                [this, self] (const boost::system::error_code& error, std::size_t) {
                  if (error) {
                    onReceiveError(error, std::move(m_recvMessage));
                  }
                  else {
                    onReceive(std::move(m_recvMessage));
                  }
                  if (--m_recvNPending != 0) {
                    startReceive(self);
                  }
                  else {
                    checkClose();
                  }
                }));
            return;
          }
          else if (m_recvHeader.size() != 0) {
            // if createMessage returns null and the message wasn't header-only, we must receive
            // the message and discard it
            uint8_t* data = new uint8_t[m_recvHeader.size()];
            boost::asio::async_read(m_socket, boost::asio::buffer(data, m_recvHeader.size()), m_strand.wrap(
                [this, self, data] (const boost::system::error_code&, std::size_t) {
                  // errors are ignored, as there is no handler method to report them to
                  delete[] data;
                  if (--m_recvNPending != 0) {
                    startReceive(self);
                  }
                  else {
                    checkClose();
                  }
                }));
            return;
          }
        }
        if (--m_recvNPending != 0) {
          startReceive(self);
        }
        else {
          checkClose();
        }
      }));
}

void Session::asyncSend(std::unique_ptr<const OutputMessage> message)
{
  auto self = shared_from_this();
  // Being non-copyable, unique_ptr cannot be used as bound parameter for a
  // lambda. Therefore, we bind the "bare" pointer to the lambda and use it in
  // the body of the lambda to construct a new unique_ptr
  auto bareMessage = message.release();
  m_strand.dispatch(
      [this, self, bareMessage] () {
        if (m_state != State::OPEN) {
          // completion method must be posted
          m_strand.post(
              [this, self, bareMessage] () {
                onSendError(Error::SESSION_NOT_OPEN, std::unique_ptr<const OutputMessage>(bareMessage));
              });
        }
        else if (bareMessage->typeId() >= RESERVED_MESSAGE_TYPES_BASE) {
          // completion method must be posted
          m_strand.post(
              [this, self, bareMessage] () {
                onSendError(Error::RESERVED_MESSAGE_TYPE, std::unique_ptr<const OutputMessage>(bareMessage));
              });
        }
        else {
          m_sendQueue.emplace_back(bareMessage);
          if (m_sendQueue.size() == 1) {
            // no previous send operations were pending: start a new one
            startSend(self);
          }
        }
      });
}

void Session::startSend(const std::shared_ptr<Session>& self)
{
  auto& message = m_sendQueue.front();

  // m_sendBuffers[0] points to m_sendHeader
  m_sendBuffers.resize(1);
  // append the message
  message->toBuffers(m_sendBuffers);
  // calculate the size
  uint32_t size = 0;
  for (auto it = m_sendBuffers.cbegin() + 1; it != m_sendBuffers.cend(); ++it) {
    size += boost::asio::buffer_size(*it);
  }
  // fill the header
  m_sendHeader.fill(message->typeId(), message->transactionId(), size);

  boost::asio::async_write(m_socket, boost::make_iterator_range(m_sendBuffers), m_strand.wrap(
      [this, self, &message] (const boost::system::error_code& error, std::size_t /* bytes */) {
        if (error) {
          onSendError(error, std::move(message));
        }
        else {
          onSend(std::move(message));
        }
        // get rid of the moved message
        m_sendQueue.pop_front();
        if (!m_sendQueue.empty()) {
          // there are outstanding messages to send: start a new send operation
          startSend(self);
        }
        else {
          checkClose();
        }
      }));
}

boost::asio::ip::tcp::endpoint Session::localEndpoint() const
{
  boost::system::error_code ignoredError;
  return m_socket.local_endpoint(ignoredError);
}

std::string Session::localName() const
{
  return m_localName;
}

boost::asio::ip::tcp::endpoint Session::remoteEndpoint() const
{
  boost::system::error_code ignoredError;
  return m_socket.remote_endpoint(ignoredError);
}

std::string Session::remoteName() const
{
  return m_remoteName;
}

Session::State Session::state() const
{
  return m_state;
}

boost::asio::io_service::strand& Session::getStrand()
{
  return m_strand;
}

} // namespace asyncmsg
} // namespace daq
