#include "asyncmsg/UDPSession.h"

#include "asyncmsg/Error.h"

#include <boost/asio/ip/multicast.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/system/error_code.hpp>          // for error_code
#include <utility>
#include <algorithm>

namespace daq
{
namespace asyncmsg
{

UDPSession::UDPSession(boost::asio::io_service& ioService, uint16_t port) :
    m_strand(ioService),
    m_socket(ioService),
    m_state(State::OPEN),
    m_recvNPending(0),
    m_sendBuffers{ boost::asio::buffer(m_sendHeader.data()) }
{
    m_socket.open(boost::asio::ip::udp::v4());

    // we always allow to re-use the address
    boost::asio::socket_base::reuse_address option(true);
    m_socket.set_option(option);

    m_socket.bind(boost::asio::ip::udp::endpoint(boost::asio::ip::udp::v4(), port));
}

UDPSession::~UDPSession()
{
}

void UDPSession::connect(const boost::asio::ip::udp::endpoint& remoteEndpoint)
{
  m_socket.connect(remoteEndpoint);
}

void UDPSession::close()
{
  m_socket.close();
  auto self = shared_from_this();
  m_strand.dispatch(
      [this, self] () {
        m_state = State::CLOSED;
      });
}

void UDPSession::asyncReceive()
{
  auto self = shared_from_this();
  m_strand.dispatch(
      [this, self] () {
        if (m_state != State::OPEN) {
          // use post to avoid stack overflows if the user calls asyncReceive again from
          // onReceiveError()
          m_strand.post(
              [this, self] () {
                onReceiveError(Error::SESSION_NOT_OPEN, std::unique_ptr<InputMessage>());
              });
        }
        else {
          if (++m_recvNPending == 1) {
            // no previous receive operations were pending: start a new one
            startReceive(self);
          }
        }
      });
}

void UDPSession::startReceive(const std::shared_ptr<UDPSession>& self)
{
  m_socket.async_receive(boost::asio::buffer(m_recvHeader.data()), boost::asio::ip::udp::socket::message_peek, m_strand.wrap(
      [this, self] (const boost::system::error_code& error, std::size_t) {
        if (error) { // check header size
          onReceiveError(error, std::unique_ptr<InputMessage>());
        }
        else {
          m_recvMessage = createMessage(
              m_recvHeader.typeId(), m_recvHeader.transactionId(), m_recvHeader.size());
          if (m_recvMessage) {
            m_recvBuffers.clear();
            // must read header again
            m_recvBuffers.push_back(boost::asio::buffer(m_recvHeader.data()));
            m_recvMessage->toBuffers(m_recvBuffers);
            m_socket.async_receive(boost::make_iterator_range(m_recvBuffers), m_strand.wrap(
                [this, self] (const boost::system::error_code& error, std::size_t) {
                  if (error) {
                    onReceiveError(error, std::move(m_recvMessage));
                  }
                  else {
                    onReceive(std::move(m_recvMessage));
                  }
                  if (--m_recvNPending != 0) {
                    startReceive(self);
                  }
                }));
            return;
          }
          else {
            // if createMessage returns null we must receive the message and discard it
            uint8_t* data = new uint8_t[m_recvHeader.size() + detail::Header::SIZE]; // add header size
            m_socket.async_receive(boost::asio::buffer(data, m_recvHeader.size() + detail::Header::SIZE), m_strand.wrap(
                [this, self, data] (const boost::system::error_code&, std::size_t) {
                  // errors are ignored, as there is no handler method to report them to
                  delete[] data;
                  if (--m_recvNPending != 0) {
                    startReceive(self);
                  }
                }));
            return;
          }
        }
        if (--m_recvNPending != 0) {
          startReceive(self);
        }
      }));
}

void UDPSession::asyncSend(std::unique_ptr<const OutputMessage> message)
{
  auto self = shared_from_this();
  // Being non-copyable, unique_ptr cannot be used as bound parameter for a
  // lambda. Therefore, we bind the "bare" pointer to the lambda and use it in
  // the body of the lambda to construct a new unique_ptr
  auto bareMessage = message.release();
  m_strand.dispatch(
      [this, self, bareMessage] () {
        if (m_state != State::OPEN) {
          // use post to avoid stack overflows if the user calls asyncSend again from onSendError()
          m_strand.post(
              [this, self, bareMessage] () {
                onSendError(Error::SESSION_NOT_OPEN, std::unique_ptr<const OutputMessage>(bareMessage));
              });
        }
        else if (bareMessage->typeId() >= RESERVED_MESSAGE_TYPES_BASE) {
          // use post to avoid stack overflows if the user calls asyncSend again from onSendError()
          m_strand.post(
              [this, self, bareMessage] () {
                onSendError(Error::RESERVED_MESSAGE_TYPE, std::unique_ptr<const OutputMessage>(bareMessage));
              });
        }
        else {
          m_sendQueue.emplace_back(bareMessage);
          if (m_sendQueue.size() == 1) {
            // no previous send operations were pending: start a new one
            startSend(self);
          }
        }
      });
}

void UDPSession::startSend(const std::shared_ptr<UDPSession>& self)
{
  auto& message = m_sendQueue.front();

  // m_sendBuffers[0] points to m_sendHeader
  m_sendBuffers.resize(1);
  // append the message
  message->toBuffers(m_sendBuffers);
  // calculate the size
  uint32_t size = 0;
  for (auto it = m_sendBuffers.cbegin() + 1; it != m_sendBuffers.cend(); ++it) {
    size += boost::asio::buffer_size(*it);
  }
  // fill the header
  m_sendHeader.fill(message->typeId(), message->transactionId(), size);

  m_socket.async_send(boost::make_iterator_range(m_sendBuffers), m_strand.wrap(
      [this, self, &message] (const boost::system::error_code& error, std::size_t /* bytes */) {
        if (error) {
          onSendError(error, std::move(message));
        }
        else {
          onSend(std::move(message));
        }
        // get rid of the moved message
        m_sendQueue.pop_front();
        if (!m_sendQueue.empty()) {
          // there are outstanding messages to send: start a new send operation
          startSend(self);
        }
      }));
}

boost::asio::ip::udp::endpoint UDPSession::localEndpoint() const
{
  boost::system::error_code ignoredError;
  return m_socket.local_endpoint(ignoredError);
}

boost::asio::ip::udp::endpoint UDPSession::remoteEndpoint() const
{
  boost::system::error_code ignoredError;
  return m_socket.remote_endpoint(ignoredError);
}

UDPSession::State UDPSession::state() const
{
  return m_state;
}

boost::asio::io_service::strand& UDPSession::getStrand()
{
  return m_strand;
}

void UDPSession::setOutgoingInterface(const boost::asio::ip::address& ip_address)
{
    boost::asio::ip::multicast::outbound_interface option(ip_address.to_v4());
    m_socket.set_option(option);

    // TTL     Scope
    // ----------------------------------------------------------------------
    // 0    Restricted to the same host. Won't be output by any interface.
    // 1    Restricted to the same subnet. Won't be forwarded by a router.
    // <32         Restricted to the same site, organization or department.
    // <64 Restricted to the same region.
    // <128 Restricted to the same continent.
    // <255 Unrestricted in scope. Global.
    boost::asio::ip::multicast::hops hops_option(31);
    m_socket.set_option(hops_option);    
}

void UDPSession::joinMulticastGroup(const boost::asio::ip::address& mc_address, const boost::asio::ip::address& network)
{
    boost::asio::ip::multicast::join_group option(mc_address.to_v4(), network.to_v4());
    m_socket.set_option(option);    
}

void UDPSession::leaveMulticastGroup(const boost::asio::ip::address& mc_address)
{
    boost::asio::ip::multicast::leave_group option(mc_address.to_v4());
    m_socket.set_option(option);
}


} // namespace asyncmsg
} // namespace daq
