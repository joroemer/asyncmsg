

#include "asyncmsg/NameService.h"
#include "Interface.h"

#include "MsgInfo.h"
#include "MultiCastGroup.h"
#include "MultiCastGroupNamed.h"

#include "ers/Issue.h"                   // for Issue
#include "ers/LocalContext.h"            // for ERS_HERE
#include "ipc/partition.h"               // for IPCPartition
#include "is/criteria.h"                 // for ISCriteria

#include "is/infostream.h"
#include "is/infodictionary.h"
#include "is/serveriterator.h"
#include "is/infoiterator.h"

#include "boost/lexical_cast.hpp"
#include <boost/asio/ip/address.hpp>     // for address
#include <boost/asio/ip/tcp.hpp>         // for tcp::endpoint, tcp

#include <net/if.h>                      // for IFF_LOOPBACK, IFF_UP
#include <netinet/in.h>                  // for htonl, in6_addr, in6addr_any
#include <stdint.h>                      // for uint8_t, uint16_t
#include <stdlib.h>                      // for getenv, size_t
#include <string.h>                      // for memcmp
#include <sys/socket.h>                  // for AF_INET, AF_INET6
#include <unistd.h>                      // for gethostname

#include <array>                         // for array
#include <algorithm>
#include <map>                           // for map, map<>::mapped_type
#include <string>                        // for string, allocator, operator+
#include <tuple>                         // for tie, get, tuple, make_tuple
#include <vector>                        // for vector


namespace daq {

    namespace asyncmsg {

        NameService::NameService(IPCPartition partition, const std::vector<std::string>& data_networks, const std::string& is_server) noexcept
            : m_partition(partition),
              m_is_server(is_server)
        {

            if(const char *server = getenv("DF_CONFIG_IS_SERVER")) {
                // override user argument
                m_is_server = server;
            }

            for(const auto & network : data_networks) {
                m_data_networks.push_back(parse_address_network(network));
            }
        }

        NameService::~NameService() noexcept
        {
            // nothing to do
        }

        // Publish IS object with all valid local interfaces and port number.
        // The 'name' parameter should typically be the application name.
        void NameService::publish(const std::string& name, uint16_t port)
        {
            MsgInfo info; 

            char host[1024];
            gethostname(host, sizeof(host));

            info.Hostname = host;
            info.Port     = port;

            const daq::asyncmsg::Interface::InterfaceList& interfaces = daq::asyncmsg::Interface::interfaces();

            for(auto intf : interfaces) {

                // check for invalid interfaces
                // !UP, LOOPBACK, address == 0 (e.g. when bonded)

                if(!intf->has_flag(IFF_UP) || intf->has_flag(IFF_LOOPBACK)) {
                    continue;
                }

                // An interface maybe up, but no IP address set, e.g. if it
                // is used in a bonded configuration.
                if(intf->family() == AF_INET && intf->address() == 0) {
                    continue;
                }

                if(intf->family() == AF_INET6) {
                    auto a = intf->address_v6();
                    if(memcmp(&a,&in6addr_any, sizeof(in6addr_any)) == 0) {
                        continue;
                    }
                }

                for(const auto& network : m_data_networks) {

                    auto& addr = std::get<0>(network);
                    auto& mask = std::get<1>(network);

                    if(addr.is_v4() && intf->family() == AF_INET) {
		        auto addr4(addr.to_v4());
			auto mask4(mask.to_v4());

                        if(htonl(addr4.to_ulong() & mask4.to_ulong()) == (intf->address() & htonl(mask4.to_ulong()))) {
			    info.Addresses.push_back(intf->address_string() + '/' +  mask4.to_string());
                        }

                    } else if(addr.is_v6() && intf->family() == AF_INET6) {
                        auto addr6(addr.to_v6().to_bytes());
                        auto mask6(mask.to_v6().to_bytes());

                        auto local_network = intf->network_v6();

                        bool matches = true;
                        for(size_t i = 0; i < addr6.size(); i++) {
                            if((addr6[i] & mask6[i]) != local_network.s6_addr[i]) {
                                matches = false;
                                break;
                            }
                        }
                        
                        if(matches) {
                            info.Addresses.push_back(intf->address_string() + '/' + intf->netmask_string());
                        }
                    } 
                }
            }

            /* XXX: hack!
             *
             * The following are the IP address prefixes currently used at P1:
             * - ATCN 10.145
             * - CTRL 10.146
             * - DATA 10.147
             * This configuration does not allow to isolate ATCN/CTRL from DATA
             * using network masks, since the smallest mask including ATCN and
             * CTRL, also includes DATA (10.144.0.0/255.252.0.0).
             *
             * This is problematic since in certain scenarios the TTC2LAN
             * client, running on an ATCN machine, needs to connect to the
             * HLTSV server, running on a CTRL+DATA machine, using the
             * ATCN-to-CTRL path, and avoiding the ATCN-to-DATA path, which is
             * closed. Sorting the addresses allows (just by chance) to achieve
             * that: using the very permissive mask described above, the HLTSV
             * will publish its CTRL and DATA addresses, in that order. The
             * TTC2LAN will connect to the first address that matches the mask,
             * i.e. the CTRL address.
             */
            std::sort(info.Addresses.begin(), info.Addresses.end());

            if(info.Addresses.empty()) {
                throw CannotPublish(ERS_HERE);
            }

            ISInfoDictionary d(m_partition);

            ISServerIterator servers(m_partition, m_is_server + ".*");
            if(servers.entries() == 0) {
                throw InvalidISServer(ERS_HERE,m_is_server);
            }
            while(servers++) {
                // may throw
                try {
                   d.checkin(std::string(servers.name()) + ".MSG_" + name, info);
                } catch(ers::Issue& reason) {
                   throw InvalidISServer(ERS_HERE, servers.name(), reason);
                }
            }

        }

        bool NameService::matches(const boost::asio::ip::address& addr, const boost::asio::ip::address& mask) const
        {
            for(const auto intf : daq::asyncmsg::Interface::interfaces()) {

                // ignore these
                if(!intf->has_flag(IFF_UP) || intf->has_flag(IFF_LOOPBACK) || intf->address() == 0) {
                    continue;
                }
                
                // compare IPV4 address
                if(addr.is_v4() && intf->family() == AF_INET) {
		    if(htonl(addr.to_v4().to_ulong() & mask.to_v4().to_ulong()) == (intf->network() & htonl(mask.to_v4().to_ulong()))) {
                        // found one, first match will be used
                        return true;
                    }
                } else if (addr.is_v6() && intf->family() == AF_INET6) {
                    auto addr6(addr.to_v6().to_bytes());
                    auto mask6(mask.to_v6().to_bytes());

                    auto local_network = intf->network_v6();

                    bool matches = true;
                    for(size_t i = 0; i < addr6.size(); i++) {
                        if((addr6[i] & mask6[i]) != local_network.s6_addr[i]) {
                            matches = false;
                            break;
                        }
                    }

                    return matches;
                }
            }
            
            // no match found at all
            return false;
        }
                
        // Look up all applications of given 'type' and return a list of endpoints
        std::vector<boost::asio::ip::tcp::endpoint> 
        NameService::lookup(const std::string& prefix) const
        {

            std::vector<boost::asio::ip::tcp::endpoint> results;

            ISInfoStream it(m_partition, m_is_server, ISCriteria("MSG_" + prefix + ".*", MsgInfo::type()));

            while(!it.eof()) {
                std::string name(it.name());
                MsgInfo     info;

                it >> info;

                // match my local interfaces against the list
                for(auto& addr_netmask : info.Addresses) {
                    boost::asio::ip::address addr, mask;
                    std::tie(addr, mask) = parse_address_network(addr_netmask);

                    if(matches(addr, mask)) {
                        results.push_back(boost::asio::ip::tcp::endpoint(addr, info.Port));
                        break;
                    }
                }
            }

            return results;
        }

        // Look up all applications of given 'type' and return a map of names and endpoints
        std::map<std::string,boost::asio::ip::tcp::endpoint> 
        NameService::lookup_names(const std::string& prefix) const
        {

            std::map<std::string,boost::asio::ip::tcp::endpoint> results;

            ISInfoStream it(m_partition, m_is_server, ISCriteria("MSG_" + prefix + ".*", MsgInfo::type()));

            while(!it.eof()) {
                std::string name(it.name());
                MsgInfo     info;

                it >> info;

                // match my local interfaces against the list
                for(auto& addr_netmask : info.Addresses) {
                    boost::asio::ip::address addr, mask;
                    std::tie(addr, mask) = parse_address_network(addr_netmask);

                    if(matches(addr, mask)) {
                        results[name.substr(m_is_server.size() + 5)] = boost::asio::ip::tcp::endpoint(addr, info.Port);
                        break;
                    }
                }
            }

            return results;
        }

        // Look up all applications of given 'type' and return a map of all names and endpoints that match
        std::map<std::string, std::vector<boost::asio::ip::tcp::endpoint>>
        NameService::lookup_all(const std::string& prefix) const
        {

            std::map<std::string,std::vector<boost::asio::ip::tcp::endpoint>> results;

            ISInfoStream it(m_partition, m_is_server, ISCriteria("MSG_" + prefix + ".*", MsgInfo::type()));

            while(!it.eof()) {
                std::string name(it.name());
                MsgInfo     info;

                it >> info;

                // match my local interfaces against the list
                for(auto& addr_netmask : info.Addresses) {
                    boost::asio::ip::address addr, mask;
                    std::tie(addr, mask) = parse_address_network(addr_netmask);

                    if(matches(addr, mask)) {
                        results[name.substr(m_is_server.size() + 5)].push_back(boost::asio::ip::tcp::endpoint(addr, info.Port));
                    }
                }
            }

            return results;
        }


        boost::asio::ip::tcp::endpoint 
        NameService::resolve(const std::string& name) const
        {
            ISInfoDictionary d(m_partition);

            MsgInfo info;

            d.getValue(m_is_server + ".MSG_" + name, info);

            for(auto& addr_netmask : info.Addresses) {
                boost::asio::ip::address addr, mask;
                std::tie(addr, mask) = parse_address_network(addr_netmask);
                
                if(matches(addr, mask)) {
                    return boost::asio::ip::tcp::endpoint(addr, info.Port);                    
                }
            }

            throw CannotResolve(ERS_HERE,name);

        }

        NameService::Network 
        NameService::parse_address_network(const std::string& network)
        {
            size_t slash = network.find('/');
            std::string address = network.substr(0, slash);
            std::string netmask = "255.255.255.255";
            if(slash != std::string::npos) {
                netmask = network.substr(slash+1);
            }

            return std::make_tuple(boost::asio::ip::address::from_string(address),
                                   boost::asio::ip::address::from_string(netmask));
        }

        boost::asio::ip::address NameService::find_interface(const std::string& netmask)
        {
            std::string local_mask(netmask);
	    std::string mc_mask("255.255.255.255");

            auto n = netmask.find('/');
            if (n != std::string::npos) {
                local_mask = netmask.substr(0,n);
                mc_mask = netmask.substr(n+1);
            }

            auto addr = boost::asio::ip::address::from_string(local_mask).to_v4();
	    auto mask = boost::asio::ip::address::from_string(mc_mask).to_v4();

            auto interfaces = daq::asyncmsg::Interface::interfaces();
            for(auto intf : interfaces) {
                if(!intf->has_flag(IFF_UP) || intf->has_flag(IFF_LOOPBACK)) {
                    continue;
                }

                if(intf->family() != AF_INET) {
                    continue;
                }

                if(intf->address() == 0) {
                    continue;
                }

                if((intf->address() & htonl(mask.to_ulong())) == htonl(addr.to_ulong())) {
                    return boost::asio::ip::address::from_string(intf->address_string());
                }
            }

            return boost::asio::ip::address();
        }

        std::string NameService::allocate_multicast_address(const std::string& addr, const std::string& network)
        {
	    const std::string    name_prefix("RunParams.MultiCast-");
	    const std::string    group_prefix("224.100.100.");

            MultiCastGroup group;

            group.Partition        = m_partition.name();
            group.MulticastAddress = addr;
            group.MulticastNetwork = network;

            if(addr == "*") {

                // Find potential entry in initial partition
                ISInfoIterator it(IPCPartition(), "RunParams", MultiCastGroup::type());
                bool found = false;

                while(it++) {
                    it.value(group);
                    if(group.Partition == m_partition.name()) {
                        // found our partition.
                        found = true;
                        break;
                    }
                }

                if(!found) {
                    // allocate a new group.

                    unsigned short index = 1;

                    group.Partition = m_partition.name();
                    group.MulticastAddress = group_prefix + boost::lexical_cast<std::string>(index);
                    group.MulticastNetwork = network;

		    ISInfoDictionary  dict(IPCPartition("initial"));

                    while(!found && index < 256) {
                        
                        try {
                            dict.insert(name_prefix + group.MulticastAddress, group);
                            found = true;
                        } catch(...) {
                            // try next
                            index++;
			    group.MulticastAddress = group_prefix + boost::lexical_cast<std::string>(index);
                        }
                    }
                }
                    
                if(!found) {
                    // A dynamic address was requested, but we could not allocate one...
                    throw CannotAllocateMulticast(ERS_HERE);
                }
            } else {
		// Statically configured address
		MultiCastGroupNamed checkGroup(IPCPartition("initial"), name_prefix + group.MulticastAddress);
		if(checkGroup.isExist()) {
		    checkGroup.checkout();

		    // The partition names must match, or there is the potential that two
		    // partitions use the same MC group.
		    if(checkGroup.Partition != group.Partition) {
			throw CannotAllocateMulticast(ERS_HERE);
		    }
		} else {
		    // publish our new object to say what we are using.
		    checkGroup.Partition        = group.Partition;
		    checkGroup.MulticastAddress = group.MulticastAddress;
		    checkGroup.MulticastNetwork = group.MulticastNetwork;
		    checkGroup.checkin();
		}
	    }

            ISInfoDictionary  dict(m_partition);
            ISServerIterator servers(m_partition, m_is_server + ".*");
            if(servers.entries() == 0) {
                throw InvalidISServer(ERS_HERE,m_is_server);
            }
            while(servers++) {
                // may throw
                try {
                    dict.checkin(std::string(servers.name()) + ".MultiCastAddress", group);
                } catch(ers::Issue& reason) {
                    throw InvalidISServer(ERS_HERE, servers.name(), reason);
                }
            }
            
            return group.MulticastAddress;
        }
        
        std::string NameService::lookup_multicast_address(const std::string& addr) const
        {
            if(addr == "*") {

                MultiCastGroupNamed group(m_partition, m_is_server + ".MultiCastAddress");
                
                try {
                    group.checkout();
                    return group.MulticastAddress;
                    
                } catch (ers::Issue& ex) {
                    throw CannotResolve(ERS_HERE, addr, ex);
                }
            }

            return addr;
        }
    }
}
