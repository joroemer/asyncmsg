#include "asyncmsg/Error.h"

namespace daq
{
namespace asyncmsg
{

ErrorCategory& ErrorCategory::instance()
{
  return s_instance;
}

const char* ErrorCategory::name() const noexcept
{
  return "asyncmsg";
}

ErrorCategory ErrorCategory::s_instance;

std::string ErrorCategory::message(int value) const
{
  if (value == static_cast<int>(Error::SESSION_NOT_OPEN)) {
    return "The session is not open";
  }
  if (value == static_cast<int>(Error::SESSION_NOT_CLOSED)) {
    return "The session is not closed";
  }
  if (value == static_cast<int>(Error::RESERVED_MESSAGE_TYPE)) {
    return "Message types greater than 0x7FFFFFFF are reserved";
  }
  if (value == static_cast<int>(Error::UNEXPECTED_MESSAGE_TYPE)) {
    return "Unexpected message type";
  }
  if (value == static_cast<int>(Error::MESSAGE_DISCARDED)) {
    return "Message discarded";
  }
  return "Unknown asyncmsg error";
}

} // namespace asyncmsg
} // namespace daq
