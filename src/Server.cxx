#include "asyncmsg/Server.h"

#include "asyncmsg/Session.h"

#include <boost/system/error_code.hpp>
#include <boost/asio/io_context.hpp>         // for io_context
#include <boost/asio/io_context_strand.hpp>  // for io_context::strand
#include <boost/asio/socket_base.hpp>        // for socket_base::reuse_address

namespace daq
{
namespace asyncmsg
{

Server::Server(boost::asio::io_service& ioService) :
    m_acceptor(ioService),
    m_strand(ioService)
{
}

Server::~Server()
{
  close();
}

void Server::listen(const std::string& localName,
    const boost::asio::ip::tcp::endpoint& localEndpoint)
{
  m_acceptor.open(localEndpoint.protocol());
  m_acceptor.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
  m_acceptor.bind(localEndpoint);
  m_localName = localName;
  m_acceptor.listen();
  m_isListening = true;
}

void Server::asyncAccept(std::shared_ptr<Session> session)
{
  startAccept(shared_from_this(), session);
}

void Server::startAccept(const std::shared_ptr<Server>& self,
    const std::shared_ptr<Session>& session)
{
  m_acceptor.async_accept(session->m_socket, m_strand.wrap(
      [this, self, session] (const boost::system::error_code& error) {
        if (error) {
          // Expect errors when closing the server while listening for connections
          if (m_isListening.load()) {
            onAcceptError(error, session);
          }
        }
        else {
          session->m_state = Session::State::OPEN_PENDING;
          session->m_localName = m_localName;
          session->startOpen(session);
          onAccept(session);
        }
      }));
}

void Server::close()
{
  m_isListening = false;
  m_acceptor.close();
  m_localName.clear();
}

boost::asio::ip::tcp::endpoint Server::localEndpoint() const
{
  boost::system::error_code ignoredError;
  return m_acceptor.local_endpoint(ignoredError);
}

std::string Server::localName() const
{
  return m_localName;
}

} // namespace asyncmsg
} // namespace daq
