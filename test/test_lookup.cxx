
// Run control related
#include "RunController/Controllable.h"
#include "RunController/ConfigurationBridge.h"
#include "RunController/ItemCtrl.h"

#include "ipc/core.h"
#include "ers/ers.h"

#include "asyncmsg/NameService.h"

#include "dal/Partition.h"
#include "DFdal/DFParameters.h"

#include <string>
#include <exception>
#include <chrono>
#include <unistd.h>

namespace {

    class LookupApp : public daq::rc::Controllable {
    public:
        LookupApp(const std::string& name)
            : daq::rc::Controllable(name)
        {}
        ~LookupApp() {}

        void connect(std::string&) 
        {
            using namespace daq::asyncmsg;
            using namespace daq::rc;

            ConfigurationBridge *bridge = ConfigurationBridge::instance();
            Configuration *config =      bridge->getConfiguration();

            IPCPartition partition(bridge->getPartition()->UID());
            NameService  name_service(partition,
                                      config->cast<daq::df::DFParameters>(bridge->getPartition()->get_DataFlowParameters())->get_DefaultDataNetworks());

            
            auto start = std::chrono::high_resolution_clock::now();

            std::vector<boost::asio::ip::tcp::endpoint> result = name_service.lookup("ROS");

            auto diff = std::chrono::high_resolution_clock::now() - start;

            ERS_LOG("Found: " << result.size() << " endpoints");
            ERS_LOG("Lookup: " << std::chrono::duration_cast<std::chrono::microseconds>(diff).count() << " microseconds");
                               
        }
    };
}

int main(int argc, char *argv[])
{
    try {
        
        IPCCore::init(argc, argv);

        std::string name;
        std::string parent;

        bool done = false;
        while(!done) {
            switch(getopt(argc, argv, "N:n:is:P:")) {
            case 'N':
                break;
            case 'n':
                name = optarg;
                break;
            case 'i':
                break;
            case 's':
                break;
            case 'P':
                parent = optarg;
            case -1:
                done = true;
                break;
            }
        }
        
        daq::rc::ItemCtrl control(new LookupApp(name), false, parent);

        control.run();
        
    } catch(ers::Issue& ex) {
        ers::fatal(ex);
        exit(EXIT_FAILURE);
    } catch(std::exception& ex) {
        std::cerr << ex.what() << std::endl;
        exit(EXIT_FAILURE);
    } catch(...) {
        std::cerr << "Unknown exception" << std::endl;
        exit(EXIT_FAILURE);
    }
}
