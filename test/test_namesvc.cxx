
#include "asyncmsg/NameService.h"

#include "ipc/core.h"
#include "ipc/partition.h"                   // for IPCPartition
#include "ers/Issue.h"                       // for operator<<, Issue

#include <iostream>
#include <cstdlib>
#include <map>                               // for map
#include <string>                            // for string, operator<<, allo...
#include <utility>                           // for pair
#include <vector>                            // for vector

int main(int argc, char *argv[])
{

    if(argc < 2) {
        std::cerr<< "usage: " << argv[0] << " partition [appname [ data_networks* ]]" << '\n'
                 << "       where data_networks is in the format IPAddress/NetMask" << std::endl;
        
        exit(EXIT_FAILURE);
    }

    std::string appname("test_app");
    if(argc > 2) {
        appname = argv[2];
    }
    
    IPCCore::init(argc, argv);

    IPCPartition partition(argv[1]);

    //
    // Some known data networks. This would normally come from
    //
    //  DFParameters.DefaultDataNetworks 
    // 
    // CERN GPN
    // Testbed B.4 pub, internal
    //
    std::vector<std::string> data_networks{"137.138.0.0/255.255.0.0", 
            "188.184.2.64/255.255.255.192",
            "10.193.128.0/255.255.254.0",
            "10.193.64.0/255.255.254.0"};

    argv += 3;
    argc -= 3;
    while(argc > 0) {
        data_networks.push_back(argv[0]);
        argc--;
        argv++;
    }
    
    daq::asyncmsg::NameService config(partition, data_networks);

    try {
        config.publish(appname, 5678);
    } catch(daq::asyncmsg::CannotPublish& ex) {
        std::cerr << ex ;
        exit(EXIT_FAILURE);
    } catch(ers::Issue& ex) {
        std::cerr << ex ;
        exit(EXIT_FAILURE);
    } catch(std::exception& ex) {
        std::cerr << ex.what();
        exit(EXIT_FAILURE);        
    } catch(...) {
        std::cerr << "Unknown exception" << std::endl;
        exit(EXIT_FAILURE);
    }

    std::cout << "Published successfully" << std::endl;

    try {
        auto results = config.lookup(appname);
        
        std::cout << "Lookup" << std::endl;
        for(auto addr : results) {
            std::cout << "addr = " << addr << std::endl;
        }

        auto results2 = config.lookup_names(appname);
        std::cout << "Lookup names" << std::endl;
        for(auto addr : results2) {
            std::cout << "name = " << addr.first << " addr = " << addr.second << std::endl;
        }

        auto results3 = config.lookup_all(appname);
        std::cout << "Lookup all" << std::endl;
        for(auto addr : results3) {
            std::cout << "name = " << addr.first;
            for(auto ep : addr.second) {
                std::cout << " addr = " << ep << " ";
            }
            std::cout << std::endl;
        }

        std::cout << "Resolve" << std::endl;
        auto addr = config.resolve(appname);
        std::cout << "addr = " << addr << std::endl;

    } catch(daq::asyncmsg::CannotResolve& ex) {
        std::cerr << ex;
        exit(EXIT_FAILURE);
    } catch(ers::Issue& ex) {
        std::cerr << ex;
        exit(EXIT_FAILURE);
    } catch(...) {
        std::cerr << "Unknown exception" << std::endl;
        exit(EXIT_FAILURE);
    }

    return (EXIT_SUCCESS);
}
