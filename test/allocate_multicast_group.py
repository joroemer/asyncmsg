#!/usr/bin/env tdaq_python
#
# Allocate a globally (i.e. Point 1 wide) unique multicast address for the current
# partition.
# 
# 
import os
import sys

from ipc import IPCPartition
from ispy import ISInfoIterator, ISInfoDynAny, ISCriteria, ISInfoDictionary

partition = os.environ['TDAQ_PARTITION']
initial   = IPCPartition('initial')

# Check if there is already a definition for this partition
group = ISInfoDynAny(initial, 'MultiCastGroup')
it = ISInfoIterator(initial, 'RunParams', ISCriteria(group.type()))
while it.next():
    it.value(group)
    if group.Partition == partition:
        # yes, just re-use it
        print group
        sys.exit(0)

base_addr = '224.100.100.'

d = ISInfoDictionary(initial)

for a in range(1, 256):

    group.Partition        = partition
    group.MulticastAddress = base_addr + str(a)
    group.MulticastNetwork = '10.149.0.0/255.255.0.0'

    try:
        d.insert('RunParams.MultiCast-' + base_addr + str(a), group)
    except:
        continue
    
    # could insert object, success
    print group
    sys.exit(0)

# could not find a free multicast group in the given range, signal error

sys.exit(1)
    
    
