
/*
 * test_udpsession
 *
 *    choose a random port on the localhost interface
 *    fork a child which sends messages to this port
 *
 * test_udpsession <network_for_multicast>
 *
 *    choose a random port
 *    fork
 *    child:
 *      select your IP address as the outgoing interface for multicast
 *      send multicast messages to a fixed multicast address and this port
 *    mother:
 *      join the fixed multicast address group.
 *    
 * test_udpsession <network_for_multicast> <port>
 *
 *    Same as above, but with a fixed port number.
 * 
 * test_udpsession <network_for_multicast> <port> <anything>
 *
 *    Same as above, but only receive messages, don't send them.
 *    This can be used to have multiple receivers with the same
 *    multicast address/port on the same machine - which actually
 *    seems to work now in Linux.
 */


#include "asyncmsg/UDPSession.h"
#include "asyncmsg/NameService.h"
#include "asyncmsg/Message.h"                // for InputMessage, OutputMessage

#include <boost/asio/buffer.hpp>             // for const_buffer, mutable_bu...
#include <boost/asio/io_service.hpp>         // for io_service
#include <boost/asio/ip/address.hpp>         // for operator<<, address
#include <boost/asio/ip/basic_endpoint.hpp>  // for operator<<, basic_endpoint
#include <boost/asio/ip/udp.hpp>             // for udp, udp::endpoint
#include <boost/system/error_code.hpp>       // for operator<<, error_code (...

#include <cstdint>                           // for uint32_t, uint16_t
#include <string>                            // for string, stoul
#include <utility>                           // for move
#include <iostream>
#include <memory>
#include <vector>

#include <unistd.h>
#include <errno.h>                           // for errno
#include <stdlib.h>                          // for exit, EXIT_FAILURE
#include <string.h>                          // for strerror


namespace {

    class Input : public daq::asyncmsg::InputMessage {
    public:

        Input(uint32_t tx, uint32_t size) :
            m_tid(tx),
            m_payload(size/sizeof(uint32_t))
        {
        }

        uint32_t typeId() const override
        {
            return 1;
        }

        uint32_t transactionId()  const override
        {
            return m_tid;
        }

        void toBuffers(std::vector<boost::asio::mutable_buffer>& buffers) override
        {
            buffers.push_back(boost::asio::buffer(m_payload));
        }

    private:
        uint32_t m_tid;
        std::vector<uint32_t> m_payload;
    };

    class Output : public daq::asyncmsg::OutputMessage {
    public:

        Output()
            : m_tid(0)
        {}
        
        uint32_t typeId() const override
        {
            return 1;
        }

        uint32_t transactionId() const override
        {
            return m_tid++;
        }

        void toBuffers(std::vector<boost::asio::const_buffer>& buffers) const override
        {
            buffers.push_back(boost::asio::buffer(m_payload));
        }

    private:
        mutable uint32_t m_tid;

        uint32_t m_payload[10];

    };

    class MySession : public daq::asyncmsg::UDPSession {
    public:
        MySession(boost::asio::io_service& service, uint16_t port = 0)
            : daq::asyncmsg::UDPSession(service, port)
        {
        }

    protected:

        // only used in mother
        std::unique_ptr<daq::asyncmsg::InputMessage> createMessage(uint32_t , uint32_t tx, uint32_t size) noexcept override 
        {
            return std::unique_ptr<Input>(new Input(tx,size));
        }

        void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> in) noexcept override 
        {
            std::unique_ptr<Input> msg(dynamic_cast<Input*>(in.release()));
            std::cout << "Received message: " << msg->transactionId() << std::endl;
            asyncReceive();
        }

        void onReceiveError(const boost::system::error_code& error, std::unique_ptr<daq::asyncmsg::InputMessage>) noexcept override 
        {
            std::cerr << "Receive error: " << error << std::endl;            
        }

        // only used in child
        void onSend(std::unique_ptr<const daq::asyncmsg::OutputMessage> msg) noexcept override 
        {
            // std::cout << "Send done" << std::endl;
            asyncSend(std::move(msg));
            
        }
        void onSendError(const boost::system::error_code& error, std::unique_ptr<const daq::asyncmsg::OutputMessage>) noexcept override 
        {
            std::cerr << "Send error: " << error << std::endl;            
        }
    };

}

int main(int argc, char *argv[])
{
    using namespace daq::asyncmsg;

    std::string multicast = "225.0.10.10";
    std::string outgoing;

    uint16_t port = 0;

    if(argc > 1) {
        // outgoing should be the network used for multicast, e.g. 137.138.0.0 for
        // a machine on the CERN GPN
        outgoing = argv[1];
    }

    if(argc > 2) {
        port = static_cast<uint16_t>(std::stoul(argv[2]));
    }

    boost::asio::io_service service;

    auto session = std::make_shared<MySession>(service, port);

    auto ep = session->localEndpoint();

    std::cout << "UDP session bound to: "  << ep << std::endl;

    switch(fork()) {
    case -1:
        // error
        std::cerr << "fork() failed" << strerror(errno) << std::endl;
        exit(EXIT_FAILURE);
        break;

    case 0:
        // child
        {
            // receiver only
            if(argc > 3) exit(0);

            auto sender = std::make_shared<MySession>(service);
            if(outgoing.empty()) {
                // connect to local host
                sender->connect(boost::asio::ip::udp::endpoint(boost::asio::ip::address_v4::loopback(), ep.port()));
            } else {

                auto addr = daq::asyncmsg::NameService::find_interface(outgoing);

                std::cerr << "Outgoing interface: " << addr << std::endl;

                // connect to multicast address
                sender->setOutgoingInterface(addr);
                sender->connect(boost::asio::ip::udp::endpoint(boost::asio::ip::address::from_string(multicast), ep.port()));
            }

            std::unique_ptr<Output> msg(new Output);
            sender->asyncSend(std::move(msg));

            service.run();
        }
        break;

    default:
        // mother

        if(!outgoing.empty()) {
            auto addr = daq::asyncmsg::NameService::find_interface(outgoing);
            std::cerr << "Multicast interface: " << addr << std::endl;
            session->joinMulticastGroup(boost::asio::ip::address::from_string(multicast),addr);
        }

        // start receiving
        session->asyncReceive();
        boost::asio::io_service::work wk(service);
        service.run();
        break;
    }

}
